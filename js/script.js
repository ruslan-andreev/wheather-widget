class WeatherWidget{
    constructor(){
        this.create();
    }
    create(){
        
        let weatherApp = document.createElement('div')
        weatherApp.classList.add('weather__app')

        let weatherTitle = document.createElement('div')
        weatherTitle.classList.add('weather__title')

        let titleHtml = `<h1>WEATHER</h1>`
        weatherTitle.innerHTML = titleHtml

        let weatherInput = document.createElement('div')
        weatherInput.classList.add('weather__input')

        let inputHtml = `<input type="text" id="weather__input" placeholder='Enter city name'/>`
        weatherInput.innerHTML = inputHtml

        let weatherWrapper = document.createElement('div')
        weatherWrapper.classList.add('weatherWrapper')

        let footer = document.createElement('footer')
        footer.classList.add('footer')
        let footerHtml = `&copy Ruslan Andreev 2021`
        footer.innerHTML = footerHtml

        document.body.appendChild(weatherApp)
        weatherApp.appendChild(weatherTitle)
        weatherApp.appendChild(weatherInput)
        weatherApp.appendChild(weatherWrapper)
        weatherApp.appendChild(footer)

        this.init()
    }

    init(){
        if(localStorage.getItem('city') == null){
            this.addEventListener()
        }else{
            this.getData(JSON.parse(localStorage.getItem('city')))
        }   
    }

    addEventListener(){
        let input = document.querySelector('#weather__input')
        if(!input) return
        input.addEventListener('keyup',(event)=>{
            if(event.code.includes('Enter') || event.code.keyCode('13')){
                let city = event.target.value
                localStorage.setItem('city', JSON.stringify(city))

               this.getData(event.target.value)
            }
        })
    }

    getData = async function (cityName){
        let url = `http://api.weatherapi.com/v1/current.json?key=27134c4d1c7d4800818192329210901&q=${cityName}&lang=ru`
        let response = await fetch(url)
        let data = await response.json()

        this.show(data)
    }
    show(data){
        
        let widget = document.querySelector('.widget')
        if(!widget){
            widget = document.createElement('div')
            widget.classList.add('widget')
        }
            let widgetHtml = `
                <div class="weather__info">
                    <p class="condition">${data.current.condition.text}</p>
                    <div class="wether__info__weather">
                        <img src='https:${data.current.condition.icon}'>
                        <p class="temp"><strong>${data.current.temp_c}</strong> &deg C</p>
                    </div>
                </div>
                <div class="city__info">
                    <h2>${data.location.name}</h2>
                    <p>${data.location.country}</p>
                </div>
            `
        widget.innerHTML = widgetHtml
        let currentWeather = document.querySelector('.weatherWrapper')
        currentWeather.appendChild(widget)
        
        this.addEventListener()
    }
}
window.addEventListener('load',()=>{
    let widget = new WeatherWidget()
})